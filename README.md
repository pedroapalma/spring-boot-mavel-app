# Spring Boot Marvel App

### Tech

For building and running the application you need:

* [Maven ](https://maven.apache.org/)
* [JDK ](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Spring Boot ](https://spring.io/projects/spring-boot)
* [git](https://git-scm.com/)
* [Thymeleaf ](https://www.thymeleaf.org/)

### Running the application locally

* Download the zip or clone the Git repository.
* Unzip the zip file (if you downloaded one)
* Open Command Prompt and Change directory (cd) to folder containing pom.xml

Run the commands:
```sh
$ ./mvnw clean install
$ ./mvnw spring-boot:run
```


