package com.test.springboot.error;

import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.error.exception.AuthenticationParamException;
import com.test.springboot.error.exception.CharacterNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@ExtendWith(SpringExtension.class)
@SpringBootTest
class RestTemplateResponseErrorHandlerTest {

  private final String URL = "https://gateway.marvel.com:443/v1/public/characters/1009610?ts=1234567890&apikey=a8d4f748770b839e913ed4be726d7aa6&hash=0608bae2a13b6728c0d713a9b023eb5d";

  @Autowired
  RestTemplate restTemplate;

  private MockRestServiceServer mockServer;

  @BeforeEach
  public void setUp() {
    mockServer = MockRestServiceServer.createServer(restTemplate);
  }

  @Test
  public void test_findCharacterByIdMarvelApi_when404Error_thenThrowCharacterNotFoundException() {
    this.mockServer
        .expect(ExpectedCount.once(), requestTo(URL))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.NOT_FOUND));

    Exception exception = Assertions.assertThrows(
        CharacterNotFoundException.class, () -> restTemplate.getForEntity(URL, CharacterDataWrapper.class, 1009610));

    String expectedMessage = "Not Found";
    String actualMessage = exception.getMessage();

    assertTrue(actualMessage.contains(expectedMessage));

    mockServer.verify();
  }

  @Test
  public void test_findCharacterByIdMarvelApi_when401Error_thenThrowAuthenticationParamException() {
    this.mockServer
        .expect(ExpectedCount.once(), requestTo(URL))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.UNAUTHORIZED));

    Assertions.assertThrows(
        AuthenticationParamException.class, () -> restTemplate.getForEntity(URL, CharacterDataWrapper.class, 1009610));

    mockServer.verify();
  }

  @Test
  public void test_findCharacterByIdMarvelApi_when409Error_thenThrowAuthenticationParamException() {
    this.mockServer
        .expect(ExpectedCount.once(), requestTo(URL))
        .andExpect(method(HttpMethod.GET))
        .andRespond(withStatus(HttpStatus.CONFLICT));

    Assertions.assertThrows(
        AuthenticationParamException.class, () -> restTemplate.getForEntity(URL, CharacterDataWrapper.class, 1009610));

    mockServer.verify();
  }
}