package com.test.springboot.respository;

import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.entity.mapper.CharacterEntityMapper;
import com.test.springboot.model.Character;
import com.test.springboot.respository.api.MarvelApiService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MarvelCharacterRepositoryImplTest {

  MarvelCharacterRepositoryImpl marvelCharacterRepository;

  @Mock
  MarvelApiService marvelApiServiceMock;

  @Mock
  CharacterEntityMapper characterEntityMapperMock;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.initMocks(this);
    marvelCharacterRepository = new MarvelCharacterRepositoryImpl(marvelApiServiceMock, characterEntityMapperMock);
  }

  @Test
  void test_findById() {
    int CHARACTER_ID = 1009610;
    final CharacterDataWrapper characterDataWrapperFake = new CharacterDataWrapper();
    final Character character = new Character();
    when(marvelApiServiceMock.findCharacterById(CHARACTER_ID)).thenReturn(characterDataWrapperFake);
    when(characterEntityMapperMock.convert(characterDataWrapperFake)).thenReturn(character);

    marvelCharacterRepository.findById(CHARACTER_ID);

    verify(marvelApiServiceMock).findCharacterById(CHARACTER_ID);
    verify(characterEntityMapperMock).convert(characterDataWrapperFake);
  }
}