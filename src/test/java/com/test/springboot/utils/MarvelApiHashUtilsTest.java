package com.test.springboot.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MarvelApiHashUtilsTest {

  MarvelApiHashUtils marvelApiHashUtils;

  @BeforeEach
  void setUp() {
    marvelApiHashUtils = new MarvelApiHashUtils();
  }

  @Test
  void test_Generate() {
    long ts = 1234567890;
    String hash = marvelApiHashUtils.generate(ts);
    assertEquals("8257cd8872f90a8901acc40c1e33ac61", hash);
  }
}