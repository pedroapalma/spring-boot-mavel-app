package com.test.springboot.service;

import com.test.springboot.model.Character;
import com.test.springboot.respository.MarvelCharacterRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MarvelCharacterServiceImpTest {

  MarvelCharacterService marvelCharacterService;

  @Mock
  MarvelCharacterRepository marvelCharacterRepositoryMock;

  @BeforeEach
  void setUp() {
    marvelCharacterService = new MarvelCharacterServiceImp(marvelCharacterRepositoryMock);
  }

  @Test
  void test_findById() {
    int CHARACTER_ID = 1009610;
    Mockito.lenient().when(marvelCharacterRepositoryMock.findById(CHARACTER_ID)).thenReturn(new Character());

    marvelCharacterService.findById(CHARACTER_ID);

    verify(marvelCharacterRepositoryMock).findById(CHARACTER_ID);
  }
}