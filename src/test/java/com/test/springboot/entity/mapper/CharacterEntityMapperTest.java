package com.test.springboot.entity.mapper;

import com.test.springboot.entity.CharacterDataContainer;
import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.entity.CharacterEntity;
import com.test.springboot.entity.Thumbnail;
import com.test.springboot.model.Character;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CharacterEntityMapperTest {

  private final int ID = 1009610;
  private final String NAME = "Spider-Man";
  private final String DESCRIPTION = "Bitten by a radioactive spider, high school student Peter Parker gained the speed, strength and powers of a spider. Adopting the name Spider-Man, Peter hoped to start a career using his new abilities. Taught that with great power comes great responsibility, Spidey has vowed to use his powers to help people.";
  private final String MODIFIED = "2019-02-06T18:06:19-0500";
  private final String PATH = "http://i.annihil.us/u/prod/marvel/i/mg/3/50/526548a343e4b";
  private final String EXTENSION = "jpg";

  private CharacterEntityMapper characterEntityMapper;

  @BeforeEach
  void setUp() {
    characterEntityMapper = new CharacterEntityMapper();
  }

  @Test
  void test_convert() {
    CharacterDataWrapper characterDataWrapperFake = createFakeCharacterDataWrapper();

    Character character = characterEntityMapper.convert(characterDataWrapperFake);

    assertEquals(character.getId(), ID);
    assertEquals(character.getName(), NAME);
    assertEquals(character.getDescription(), DESCRIPTION);
    assertEquals(character.getModified(), MODIFIED);
    assertEquals(character.getImageUrl(), PATH + "." + EXTENSION);
  }

  private CharacterDataWrapper createFakeCharacterDataWrapper() {
    CharacterEntity characterEntity = new CharacterEntity();
    characterEntity.setId(ID);
    characterEntity.setName(NAME);
    characterEntity.setDescription(DESCRIPTION);
    characterEntity.setModified(MODIFIED);
    Thumbnail thumbnail = new Thumbnail();
    thumbnail.setPath(PATH);
    thumbnail.setExtension(EXTENSION);
    characterEntity.setThumbnail(thumbnail);
    List<CharacterEntity> characterEntityList = new ArrayList<>();
    characterEntityList.add(characterEntity);
    CharacterDataContainer characterDataContainer = new CharacterDataContainer();
    characterDataContainer.setCharacterEntities(characterEntityList);
    CharacterDataWrapper characterDataWrapper = new CharacterDataWrapper();
    characterDataWrapper.setCharacterDataContainer(characterDataContainer);
    return characterDataWrapper;
  }
}