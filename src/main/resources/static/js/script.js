$(function(){
    let frm = $('#heroes');

    $("#heroId").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {        
            return false;
        } 
    });

    frm.submit(function (e) {

      let heroId = $('#heroId').val();
      heroId = heroId.replace(/\s/g, '');

      e.preventDefault();

      if(heroId){
        $.ajax({
            type: frm.attr('method'),
            url: frm.attr('action')+heroId,
            success: function (data) {
                console.log('Submission was successful.');
                console.log(data);
  
                $('#heroName').val(data.name);
                $('#heroDescription').val(data.description);
                $('#heroModified').val(data.modified);
                $('#heroImg').attr('src', data.imageUrl);
                
            },
            error: function (error, ajaxOptions, thrownError) {
                console.log('An error occurred.');
                console.log(error);
                const err = eval("(" + error.responseText + ")");
                $("#errorMessage").text(err.error).fadeIn(300).delay(1500).fadeOut(250);
  
            },
        });
      }else{
        $("#errorMessage").text('Please, verify the ID provided').fadeIn(700).delay(300).fadeOut(350);
      }
    });

  });