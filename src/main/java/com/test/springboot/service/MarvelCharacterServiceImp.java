package com.test.springboot.service;

import com.test.springboot.model.Character;
import com.test.springboot.respository.MarvelCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("UnnecessaryLocalVariable")
public class MarvelCharacterServiceImp implements MarvelCharacterService {

  private MarvelCharacterRepository marvelCharacterRepository;

  @Autowired
  public MarvelCharacterServiceImp(MarvelCharacterRepository marvelCharacterRepository) {
    this.marvelCharacterRepository = marvelCharacterRepository;
  }

  @Override
  public Character findById(int id) {
    Character character = marvelCharacterRepository.findById(id);
    return character;
  }
}
