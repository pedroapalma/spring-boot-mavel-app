package com.test.springboot.service;

import com.test.springboot.model.Character;

public interface MarvelCharacterService {

  Character findById(int id);
}
