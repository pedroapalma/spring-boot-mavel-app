package com.test.springboot;

import com.test.springboot.error.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

@SpringBootApplication
public class Application {

  @Value("${marvel.api.baseurl}")
  private String baseUrl;

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder
        .errorHandler(new RestTemplateResponseErrorHandler())
        .uriTemplateHandler(new DefaultUriBuilderFactory(baseUrl))
        .build();
  }
}
