package com.test.springboot.entity.mapper;

import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.entity.CharacterEntity;
import com.test.springboot.entity.Thumbnail;
import com.test.springboot.model.Character;
import org.springframework.stereotype.Component;

@Component
public class CharacterEntityMapper {

  public Character convert(CharacterDataWrapper characterDataWrapper) {
    Character character = null;
    if (characterDataWrapper != null && characterDataWrapper.getCharacterDataContainer() != null &&
        !characterDataWrapper.getCharacterDataContainer().getCharacterEntities().isEmpty()) {
      CharacterEntity characterEntity = characterDataWrapper.getCharacterDataContainer().getCharacterEntities().get(0);
      character = new Character();
      character.setId(characterEntity.getId());
      character.setName(characterEntity.getName());
      character.setDescription(characterEntity.getDescription());
      character.setModified(characterEntity.getModified());

      Thumbnail thumbnail = characterEntity.getThumbnail();
      if (thumbnail != null && thumbnail.getPath() != null && thumbnail.getExtension() != null) {
        character.setImageUrl(thumbnail.getPath() + "." + thumbnail.getExtension());
      }
    }

    return character;
  }
}
