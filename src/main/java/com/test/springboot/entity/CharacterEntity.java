
package com.test.springboot.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "description",
    "modified",
    "thumbnail",
    "resourceURI",
    "comics",
    "series",
    "stories",
    "events",
    "urls"
})
public class CharacterEntity {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("modified")
    private String modified;
    @JsonProperty("thumbnail")
    private Thumbnail thumbnail;
    @JsonProperty("resourceURI")
    private String resourceURI;
    @JsonProperty("comics")
    private ComicsEntity comicsEntity;
    @JsonProperty("series")
    private SeriesEntity seriesEntity;
    @JsonProperty("stories")
    private StoriesEntity storiesEntity;
    @JsonProperty("events")
    private EventsEntity eventsEntity;
    @JsonProperty("urls")
    private List<Url> urls = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("modified")
    public String getModified() {
        return modified;
    }

    @JsonProperty("modified")
    public void setModified(String modified) {
        this.modified = modified;
    }

    @JsonProperty("thumbnail")
    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    @JsonProperty("thumbnail")
    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    @JsonProperty("resourceURI")
    public String getResourceURI() {
        return resourceURI;
    }

    @JsonProperty("resourceURI")
    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    @JsonProperty("comics")
    public ComicsEntity getComicsEntity() {
        return comicsEntity;
    }

    @JsonProperty("comics")
    public void setComicsEntity(ComicsEntity comicsEntity) {
        this.comicsEntity = comicsEntity;
    }

    @JsonProperty("series")
    public SeriesEntity getSeriesEntity() {
        return seriesEntity;
    }

    @JsonProperty("series")
    public void setSeriesEntity(SeriesEntity seriesEntity) {
        this.seriesEntity = seriesEntity;
    }

    @JsonProperty("stories")
    public StoriesEntity getStoriesEntity() {
        return storiesEntity;
    }

    @JsonProperty("stories")
    public void setStoriesEntity(StoriesEntity storiesEntity) {
        this.storiesEntity = storiesEntity;
    }

    @JsonProperty("events")
    public EventsEntity getEventsEntity() {
        return eventsEntity;
    }

    @JsonProperty("events")
    public void setEventsEntity(EventsEntity eventsEntity) {
        this.eventsEntity = eventsEntity;
    }

    @JsonProperty("urls")
    public List<Url> getUrls() {
        return urls;
    }

    @JsonProperty("urls")
    public void setUrls(List<Url> urls) {
        this.urls = urls;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
