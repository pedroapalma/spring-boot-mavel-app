package com.test.springboot.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;

@Component
public class MarvelApiHashUtils {

  @Value("${marvel.api.publickey}")
  private String publicKey;

  @Value("${marvel.api.privatekey}")
  private String privateKey;

  public String generate(long ts) {
    return generate(ts, privateKey, publicKey);
  }

  private String generate(long ts, String privateKey, String publicKey) {
    String code = ts + privateKey + publicKey;
    return DigestUtils.md5DigestAsHex(code.getBytes(StandardCharsets.UTF_8));
  }
}
