package com.test.springboot.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MarvelApiUrlUtils {

  @Value("${marvel.api.publickey}")
  private String publicKey;

  private MarvelApiHashUtils marvelApiHashUtils;

  @Autowired
  public MarvelApiUrlUtils(MarvelApiHashUtils marvelApiHashUtils) {
    this.marvelApiHashUtils = marvelApiHashUtils;
  }

  public String getAuthenticationUrlParams(long ts) {
    return "ts=" + ts + "&apikey=" + publicKey + "&hash=" + marvelApiHashUtils.generate(ts);
  }
}
