package com.test.springboot.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@SuppressWarnings("UnnecessaryLocalVariable")
public class MainController {

  @RequestMapping("/index")
  public String main() {
    return "app";
  }

}
