package com.test.springboot.web.controller;

import com.test.springboot.error.CustomErrorResponse;
import com.test.springboot.error.exception.AuthenticationParamException;
import com.test.springboot.error.exception.CharacterNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.UnexpectedException;

@ControllerAdvice
class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(CharacterNotFoundException.class)
  public ResponseEntity<CustomErrorResponse> handleNotFound(Exception ex, WebRequest request) {
    CustomErrorResponse errors = new CustomErrorResponse();
    errors.setError("Character Not Found");
    errors.setStatus(HttpStatus.NOT_FOUND.value());
    return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(AuthenticationParamException.class)
  public ResponseEntity<CustomErrorResponse> handleAuthenticationParamException(HttpServletResponse response) throws IOException {
    CustomErrorResponse errors = new CustomErrorResponse();
    errors.setError("Public key or Hash is missing or invalid");
    errors.setStatus(HttpStatus.UNAUTHORIZED.value());
    return new ResponseEntity<>(errors, HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(UnexpectedException.class)
  public ResponseEntity<CustomErrorResponse> handleUnexpectedException(HttpServletResponse response) throws IOException {
    CustomErrorResponse errors = new CustomErrorResponse();
    errors.setError("Unexpected Error, try again.!");
    return new ResponseEntity<>(errors, HttpStatus.NOT_IMPLEMENTED);
  }
}
