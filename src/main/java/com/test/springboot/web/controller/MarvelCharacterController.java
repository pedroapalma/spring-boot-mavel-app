package com.test.springboot.web.controller;

import com.test.springboot.model.Character;
import com.test.springboot.service.MarvelCharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings("UnnecessaryLocalVariable")
public class MarvelCharacterController {

  @Autowired
  MarvelCharacterService marvelCharacterService;

  @RequestMapping("/characters/{id:[0-9]+}")
  public Character hello(@PathVariable int id) {
    Character character = marvelCharacterService.findById(id);
    return character;
  }


}
