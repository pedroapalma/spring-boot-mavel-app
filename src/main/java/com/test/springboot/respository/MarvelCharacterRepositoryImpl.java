package com.test.springboot.respository;

import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.entity.mapper.CharacterEntityMapper;
import com.test.springboot.model.Character;
import com.test.springboot.respository.api.MarvelApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("UnnecessaryLocalVariable")
public class MarvelCharacterRepositoryImpl implements MarvelCharacterRepository {

  private MarvelApiService marvelApiService;

  private CharacterEntityMapper characterEntityMapper;

  @Autowired
  public MarvelCharacterRepositoryImpl(MarvelApiService marvelApiService, CharacterEntityMapper characterEntityMapper) {
    this.marvelApiService = marvelApiService;
    this.characterEntityMapper = characterEntityMapper;
  }

  @Override
  public Character findById(int id) {
    CharacterDataWrapper characterDataWrapper =  marvelApiService.findCharacterById(id);
    Character character = characterEntityMapper.convert(characterDataWrapper);
    return character;
  }
}
