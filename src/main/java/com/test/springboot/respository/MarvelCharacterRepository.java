package com.test.springboot.respository;

import com.test.springboot.model.Character;

public interface MarvelCharacterRepository {

  Character findById(int id);
}
