package com.test.springboot.respository.api;

import com.test.springboot.entity.CharacterDataWrapper;

public interface MarvelApiService {

  CharacterDataWrapper findCharacterById(int id);
}
