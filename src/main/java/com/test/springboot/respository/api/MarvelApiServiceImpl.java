package com.test.springboot.respository.api;

import com.test.springboot.entity.CharacterDataWrapper;
import com.test.springboot.utils.MarvelApiUrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;

@Component
@SuppressWarnings("UnnecessaryLocalVariable")
public class MarvelApiServiceImpl implements MarvelApiService {

  private MarvelApiUrlUtils marvelApiUrlUtils;

  private RestTemplate restTemplate;

  @Autowired
  public MarvelApiServiceImpl(MarvelApiUrlUtils marvelApiUrlUtils, RestTemplate restTemplate) {
    this.marvelApiUrlUtils = marvelApiUrlUtils;
    this.restTemplate = restTemplate;
  }

  @Override
  public CharacterDataWrapper findCharacterById(int id) {
    String url = "/characters/{id}?" + marvelApiUrlUtils.getAuthenticationUrlParams(Instant.now().toEpochMilli());
    ResponseEntity<CharacterDataWrapper> response = restTemplate.getForEntity(url, CharacterDataWrapper.class, id);
    if(response.getStatusCode() == HttpStatus.OK) {
      CharacterDataWrapper characterDataWrapper = response.getBody();
      return characterDataWrapper;
    } else {
      return null;
    }
  }
}
