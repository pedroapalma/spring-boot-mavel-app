package com.test.springboot.error.exception;

public class AuthenticationParamException extends RuntimeException {

  public AuthenticationParamException() {
  }

  public AuthenticationParamException(String message) {
    super(message);
  }

  public AuthenticationParamException(String message, Throwable cause) {
    super(message, cause);
  }
}
